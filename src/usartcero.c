#include <stdint.h>
#include "em_device.h"
#include "em_chip.h"
#include "em_emu.h"
#include "em_cmu.h"
#include "em_gpio.h"
#include "em_usart.h"

#include "em_wdog.h"
#include "usartcero.h"

///* Declare some strings */
//const char     welcomeString[]  = "EFM32 RS-232 - Please press a key\n";
const char     overflowString0[] = "\n---RX OVERFLOW---\n";
//const uint32_t welLen           = sizeof(welcomeString) - 1;
const uint32_t ofsLen0           = sizeof(overflowString0) - 1;

volatile struct circularBuffer0
{
  uint8_t  data[BUFFERSIZE];  /* data buffer */
  uint32_t rdI;               /* read index */
  uint32_t wrI;               /* write index */
  uint32_t pendingBytes;      /* count of how many bytes are not yet handled */
  bool     overflow;          /* buffer overflow indicator */
} rxBuf0, txBuf0 = { {0}, 0, 0, 0, false };


/* Setup UART2 in async mode for RS232*/
static USART_TypeDef           * Usart1_Port   = USART1;
static USART_InitAsync_TypeDef uartInit = USART_INITASYNC_DEFAULT;


void checarOverflow0(void)
{

 /* Check if RX buffer has overflowed */
    if (rxBuf0.overflow)
    {
      rxBuf0.overflow = false;
      uartPutData0((uint8_t*) overflowString0, ofsLen0);
    }

    /* Check if termination character is received */
    if (rxBuf0.data[(rxBuf0.wrI - 1) % BUFFERSIZE] == TERMINATION_CHAR)
    {      
      /* Copy received data to UART transmit queue */
      uint8_t tmpBuf[BUFFERSIZE];
      int     len = uartGetData0(tmpBuf, 0);
      uartPutData0(tmpBuf, len);            
    }   
}




/******************************************************************************
* @brief  uartSetup function
*
******************************************************************************/
void uartSetup0(void)
{
  /* Enable clock for GPIO module (required for pin configuration) */
  CMU_ClockEnable(cmuClock_GPIO, true);
  /* Configure GPIO pins */
  GPIO_PinModeSet(gpioPortC, 0, gpioModePushPull, 1);   //TX
  GPIO_PinModeSet(gpioPortC, 1, gpioModeInput, 0);      //RX

  GPIO_PinModeSet(gpioPortC,10,gpioModePushPull,1);

  /* Prepare struct for initializing UART in asynchronous mode*/
  uartInit.enable       = usartDisable;   /* Don't enable UART upon intialization */
  uartInit.refFreq      = 0;              /* Provide information on reference frequency. When set to 0, the reference frequency is */
  uartInit.baudrate     = 9600;           /* Baud rate */
  uartInit.oversampling = usartOVS16;     /* Oversampling. Range is 4x, 6x, 8x or 16x */
  uartInit.databits     = usartDatabits8; /* Number of data bits. Range is 4 to 10 */
  uartInit.parity       = usartNoParity;  /* Parity mode */
  uartInit.stopbits     = usartStopbits1; /* Number of stop bits. Range is 0 to 2 */
  uartInit.mvdis        = false;          /* Disable majority voting */
  uartInit.prsRxEnable  = false;          /* Enable USART Rx via Peripheral Reflex System */
  uartInit.prsRxCh      = usartPrsRxCh0;  /* Select PRS channel if enabled */

  /* Initialize USART with uartInit struct */
  USART_InitAsync(Usart1_Port, &uartInit);

  /* Prepare UART Rx and Tx interrupts */
  USART_IntClear(Usart1_Port, _USART_IFC_MASK);
  USART_IntEnable(Usart1_Port, USART_IEN_RXDATAV);
  NVIC_ClearPendingIRQ(USART1_RX_IRQn);
  NVIC_ClearPendingIRQ(USART1_TX_IRQn);
  NVIC_EnableIRQ(USART1_RX_IRQn);
  NVIC_EnableIRQ(USART1_TX_IRQn);

  /* Enable I/O pins at UART2 location #2 */
  Usart1_Port->ROUTE = USART_ROUTE_RXPEN | USART_ROUTE_TXPEN | USART_ROUTE_LOCATION_LOC0;

  /* Enable UART */
  USART_Enable(Usart1_Port, usartEnable);
}

/******************************************************************************
 * @brief  uartGetChar function
 *
 *  Note that if there are no pending characters in the receive buffer, this
 *  function will hang until a character is received.
 *
 *****************************************************************************/
uint8_t uartGetChar0()
{
  uint8_t ch;

/* Check if there is a byte that is ready to be fetched. If no byte is ready, wait for incoming data */
  if (rxBuf0.pendingBytes < 1)
  {
    while (rxBuf0.pendingBytes < 1) ;
  }

  /* Copy data from buffer */
  ch        = rxBuf0.data[rxBuf0.rdI];
  rxBuf0.rdI = (rxBuf0.rdI + 1) % BUFFERSIZE;

  /* Decrement pending byte counter */
  rxBuf0.pendingBytes--;

  return ch;
}

/******************************************************************************
 * @brief  uartPutChar function
 *
 *****************************************************************************/
void uartPutChar0(uint8_t ch)
{
  /* Check if Tx queue has room for new data */
  if ((txBuf0.pendingBytes + 1) > BUFFERSIZE)
  {
    /* Wait until there is room in queue */
    while ((txBuf0.pendingBytes + 1) > BUFFERSIZE) ;
  }

  /* Copy ch into txBuffer */
  txBuf0.data[txBuf0.wrI] = ch;
  txBuf0.wrI             = (txBuf0.wrI + 1) % BUFFERSIZE;

  /* Increment pending byte counter */
  txBuf0.pendingBytes++;

  /* Enable interrupt on USART TX Buffer*/
  USART_IntEnable(Usart1_Port, USART_IEN_TXBL);
}

/******************************************************************************
 * @brief  uartPutData function
 *
 *****************************************************************************/
void uartPutData0(uint8_t * dataPtr, uint32_t dataLen)
{
  uint32_t i = 0;

  /* Check if buffer is large enough for data */
  if (dataLen > BUFFERSIZE)
  {
    /* Buffer can never fit the requested amount of data */
    return;
  }

  /* Check if buffer has room for new data */
  if ((txBuf0.pendingBytes + dataLen) > BUFFERSIZE)
  {
    /* Wait until room */
    while ((txBuf0.pendingBytes + dataLen) > BUFFERSIZE) ;
  }

  /* Fill dataPtr[0:dataLen-1] into txBuffer */
  while (i < dataLen)
  {
    txBuf0.data[txBuf0.wrI] = *(dataPtr + i);
    txBuf0.wrI             = (txBuf0.wrI + 1) % BUFFERSIZE;
    i++;
  }

  /* Increment pending byte counter */
  txBuf0.pendingBytes += dataLen;

  /* Enable interrupt on USART TX Buffer*/
  USART_IntEnable(Usart1_Port, USART_IEN_TXBL);
}

/******************************************************************************
 * @brief  uartGetData function
 *
 *****************************************************************************/
uint32_t uartGetData0(uint8_t * dataPtr, uint32_t dataLen)
{
  uint32_t i = 0;
  memset ((char*)rxBuf0.data,'\0',BUFFERSIZE);
  
  /* Wait until the requested number of bytes are available */
  if (rxBuf0.pendingBytes < dataLen)
  {
    int counter = 0;
    
      while (rxBuf0.pendingBytes < dataLen){
        WDOG_Feed();
       counter++;
      if (counter >= 6000000){
      rxBuf0.pendingBytes++;
      strcat((char *)rxBuf0.data,"\0" );
      
      }
      }
  }
  
  if (dataLen == 0)
  {
    dataLen = rxBuf0.pendingBytes;       
    //i = rxBuf0.pendingBytes; //salvada de vida javier
  }
    for(int32_t i=0;i<30000;i++);  //Delay para eliminar caracteres extras e innecesarios
  /* Copy data from Rx buffer to dataPtr */
  while (i < dataLen)
  {
    *(dataPtr + i) = rxBuf0.data[rxBuf0.rdI];
    rxBuf0.rdI      = (rxBuf0.rdI + 1) % BUFFERSIZE;
    i++;
  }
  
  rxBuf0.rdI = 0;
  rxBuf0.wrI = 0;  
  rxBuf0.pendingBytes = 0;
  
  /* Decrement pending byte counter */
//  rxBuf0.pendingBytes -= dataLen;

  return i;
}

/***************************************************************************//**
 * @brief Set up Clock Management Unit
 ******************************************************************************/
void cmuSetup0(void)
{
  /* Start HFXO and wait until it is stable */
  /* CMU_OscillatorEnable( cmuOsc_HFXO, true, true); */

  /* Select HFXO as clock source for HFCLK */
   /*CMU_ClockSelectSet(cmuClock_HF, cmuSelect_HFXO ); */
  CMU_ClockEnable( cmuClock_CORELE, true);

  /* Disable HFRCO */
  /* CMU_OscillatorEnable( cmuOsc_HFRCO, false, false ); */

  /* Enable clock for HF peripherals */
  CMU_ClockEnable(cmuClock_HFPER, true);

  /* Enable clock for USART module */
  CMU_ClockEnable(cmuClock_USART1, true);
}

/**************************************************************************//**
 * @brief UART1 RX IRQ Handler
 *
 * Set up the interrupt prior to use
 *
 * Note that this function handles overflows in a very simple way.
 *
 *****************************************************************************/
void USART1_RX_IRQHandler(void)
{
  /* Check for RX data valid interrupt */
  if (Usart1_Port->IF & USART_IF_RXDATAV)
  {
    /* Copy data into RX Buffer */
    uint8_t rxData = USART_Rx(Usart1_Port);
    rxBuf0.data[rxBuf0.wrI] = rxData;
    rxBuf0.wrI             = (rxBuf0.wrI + 1) % BUFFERSIZE;
    rxBuf0.pendingBytes++;

    /* Flag Rx overflow */
    if (rxBuf0.pendingBytes > BUFFERSIZE)
    {
      rxBuf0.overflow = true;
    }
  }
}

/**************************************************************************//**
 * @brief UART1 TX IRQ Handler
 *
 * Set up the interrupt prior to use
 *
 *****************************************************************************/
void USART1_TX_IRQHandler(void)
{
  /* Check TX buffer level status */
  if (Usart1_Port->IF & USART_IF_TXBL)
  {
    if (txBuf0.pendingBytes > 0)
    {
      /* Transmit pending character */
      USART_Tx(Usart1_Port, txBuf0.data[txBuf0.rdI]);
      txBuf0.rdI = (txBuf0.rdI + 1) % BUFFERSIZE;
      txBuf0.pendingBytes--;
    }

    /* Disable Tx interrupt if no more bytes in queue */
    if (txBuf0.pendingBytes == 0)
    {
      USART_IntDisable(Usart1_Port, USART_IEN_TXBL);
    }
  }
}