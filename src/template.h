/*
 * XXXX.h
 *
 *  Created on: DD/MM/YYYY
 *      Author: hydro
 */

#ifndef FILELOCATION_XXXX_H_
#define FILELOCATION_XXXX_H_

/* Libraries to be included */
#include "masterSetup.h"

/* Function prototypes of the source file this header file references to */
void adc_conf();
int16_t adc_read();




#endif /* FILELOCATION_XXXX_H_ */
