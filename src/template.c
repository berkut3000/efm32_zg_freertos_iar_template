/******************************************************************************
 * @ file XXXXX.c
 *
 *  Created on: DD/MM/YYYY
 *      Author: hydro
 ******************************************************************************/

/* Headers */
#include <Folder/XXXXX.h>

/* Functions */

/***************************************************************************//**
 * @brief
 *   Brief description of the function.
 *
 * @details
 *   Detailed descritpion of the function.
 *
 * @note
 *   Comments for things that may not be intuitive.
 *
 * @param[in] dataString
 *   Values passed as parameters to the function.
 *
 * @return
 *   What the function yields.
 ******************************************************************************/
void foo()
{

}


/***************************************************************************//**
 * @brief
 *   Brief description of the function.
 *
 * @details
 *   Detailed descritpion of the function.
 *
 * @note
 *   Comments for things that may not be intuitive.
 *
 * @param[in] dataString
 *   Values passed as parameters to the function.
 *
 * @return
 *   What the function yields.
 ******************************************************************************/
int16_t bar(*param 1, *param 2)
{

}
