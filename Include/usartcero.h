#ifndef  USARTCERO_H
#define  USARTCERO_H

#include <stdint.h>
#include "em_device.h"
#include "em_chip.h"
#include "em_emu.h"
#include "em_cmu.h"
#include "em_gpio.h"
#include "em_usart.h"
//#include "bsp.h"

/* Define termination character */
#define TERMINATION_CHAR    '.'
/* Declare a circular buffer structure to use for Rx and Tx queues */
#define BUFFERSIZE          256


/* Function prototypes */
void checarOverflow0(void);

void uartSetup0(void);
void cmuSetup0(void);
void uartPutData0(uint8_t * dataPtr, uint32_t dataLen);
uint32_t uartGetData0(uint8_t * dataPtr, uint32_t dataLen);
void    uartPutChar0(uint8_t charPtr);
uint8_t uartGetChar0(void);

#endif